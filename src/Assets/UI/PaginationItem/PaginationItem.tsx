import React from 'react';
import { Pagination } from 'react-bootstrap';

interface PaginationItemProps {
  clickPage: (page: number) => void;
  page: number;
}

const paginationItem: React.FC<PaginationItemProps> = props => {
  return (
    <Pagination.Item
      onClick={() => props.clickPage(props.page)}>
      {props.page}
    </Pagination.Item>
  )
}

export default paginationItem;