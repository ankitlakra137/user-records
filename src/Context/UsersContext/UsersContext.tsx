import React, {
  useState,
  useContext,
  createContext,
  useEffect,
  ReactNode,
} from "react";

import Axios from "axios";

// INTERFACE

import { Idata } from "./UserDataInterface.models";

// CUSTOM LIBRARY
import { myFilter, mySort } from "array-sort-filter";

interface IProps {
  children: ReactNode;
}

interface ICallback {
  callback: () => null;
}

// CONTEXTS
const DataContext = createContext({
  data: [],
  subData: [],
  sortType: "",
  error: "",
  query: "",
  isLoading: true,
} as Idata);
const UpdateDataContext = createContext({});
const SortDataContext = createContext((field: string) => {});
const SearchDataContext = createContext((query: string) => {});

// CUSTOM HOOKS TO USE CONTEXT
export const useData = () => {
  return useContext(DataContext);
};

export const useDataUpdate = () => {
  return useContext(UpdateDataContext);
};

export const useDataSort = () => {
  return useContext(SortDataContext);
};

export const useDataSearch = () => {
  return useContext(SearchDataContext);
};

// Provider
export const DataProvider = (props: IProps) => {
  const [data, setData] = useState<Idata>({
    data: [],
    subData: [],
    sortType: "",
    error: "",
    query: "",
    isLoading: true,
  });

  // HANDLERS
  const updateDataHandler = (newData: any[]) => {
    setData({
      ...data,
      data: newData,
    });
  };

  const sortDataHandler = (field: string) => {
    setData(
      data.subData.length === 0
        ? {
            ...data,
            data: sortDataHelper(data.data, data.sortType, field),
            sortType:
              data.sortType === null || data.sortType === "descending"
                ? "ascending"
                : "descending",
          }
        : {
            ...data,
            subData: sortDataHelper(data.subData, data.sortType, field),
            sortType:
              data.sortType === null || data.sortType === "descending"
                ? "ascending"
                : "descending",
          }
    );
  };
  const sortDataHelper = (dataToSort: any[], type: string, field: string) => {
    return mySort(dataToSort, (a, b) => {
      if (type === null || type === "descending") {
        if (a[field] < b[field]) return -1;
        if (a[field] > b[field]) return 1;
        return 0;
      } else {
        if (a[field] > b[field]) return -1;
        if (a[field] < b[field]) return 1;
        return 0;
      }
    });
    // dataToSort.sort((a, b) => {
    //   if (type === null || type === "descending") {
    //     if (a[field] < b[field])
    //       return -1;
    //     if (a[field] > b[field])
    //       return 1;
    //     return 0;
    //   }
    //   else {
    //     if (a[field] > b[field])
    //       return -1;
    //     if (a[field] < b[field])
    //       return 1;
    //     return 0;
    //   }
    // })
    // return dataToSort;
  };

  const searchDataHandler = (query: string) => {
    if (query === "") {
      setData({
        ...data,
        subData: [],
        query: query,
      });
    } else {
      const subData = myFilter(data.data, (user) => {
        if (
          user.first_name.toLowerCase().includes(query.toLowerCase()) ||
          user.last_name.toLowerCase().includes(query.toLowerCase())
        ) {
          return user;
        }
      });
      // const subData = data.data.filter(user => {
      //   if (user.first_name.toLowerCase().includes(query.toLowerCase()) || user.last_name.toLowerCase().includes(query.toLowerCase())) {
      //     return user;
      //   }
      // })
      setData({
        ...data,
        subData: subData,
        query: query,
      });
    }
  };

  // API call after first render
  useEffect(() => {
    if (data?.data?.length) return;
    Axios.get(
      "https://datapeace-storage.s3-us-west-2.amazonaws.com/dummy_data/users.json"
    )
      .then((response) => {
        // console.log(response.data);
        setData({
          ...data,
          data: response.data,
          error: "",
          isLoading: false,
        });
      })
      .catch((error) => {
        console.log(error);
        setData({
          ...data,
          error: error.message,
          isLoading: false,
        });
      });
  }, [data]);

  // WRAP ALL COMPONENTS IN CONTEXT
  return (
    <DataContext.Provider value={data}>
      <UpdateDataContext.Provider value={updateDataHandler}>
        <SortDataContext.Provider value={sortDataHandler}>
          <SearchDataContext.Provider value={searchDataHandler}>
            {props.children}
          </SearchDataContext.Provider>
        </SortDataContext.Provider>
      </UpdateDataContext.Provider>
    </DataContext.Provider>
  );
};
