export interface Iuser{
  id: number;
  first_name: string;
  last_name: string;
  company_name: string;
  city: string;
  state: string;
  zip: number,
  email: string,
  web: string;
  age: number;
}

export interface Idata{
  data: Iuser[];
  subData: Iuser[];
  sortType: string;
  error: string;
  query: string;
  isLoading: boolean;
}