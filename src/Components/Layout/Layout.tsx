import React, { Fragment } from 'react'

// Components
import Toolbar from '../Toolbar/Toolbar';
import Body from '../Body/Body';

const layout: React.FC = () => {
  return (
    <Fragment>
      <Toolbar />
      <Body />
    </Fragment>
  )
}

export default layout;