import React from 'react';

interface Props{
  label: string;
  value: string | number;
}

const userData: React.FC<Props> = props => {
  return (
    <div>
      <span>{props.label}</span>
      <span>: </span>
      <span><strong>{props.value}</strong></span>
    </div>
  )
}

export default userData;