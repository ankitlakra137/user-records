import React from 'react';

// Style
import Styles from './Toolbar.module.css';

const toolbar: React.FC = () => {
  return (
    <div className={Styles.Toolbar}>
      <h2 className={Styles.Logo}>User Records</h2>
    </div>
  )
}

export default toolbar;